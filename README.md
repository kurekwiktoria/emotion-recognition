# Emotion Recognition

Emotion Recognition with Convolutional Neural Networks

#Instruction:

**start application:**
*python model.py --mode display*

**train model:**
*python model.py --mode train*

**start application with photo:**
*python model.py --mode foto --path name.jpg*

**exit:** 
q