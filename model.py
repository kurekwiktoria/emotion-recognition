import numpy as np
import argparse
import cv2
import keras
from keras.models import Sequential
from keras.layers.core import Dense, Dropout, Flatten
from keras.layers.convolutional import Conv2D
from keras.optimizers import Adam
from keras.layers.pooling import AveragePooling2D
from keras.layers.pooling import MaxPooling2D
from keras.preprocessing.image import ImageDataGenerator
import os
import matplotlib as mpl
import matplotlib.pyplot as plt
from sklearn.metrics import confusion_matrix
import seaborn as sns


os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
mpl.use('TkAgg')

# command line argument
ap = argparse.ArgumentParser()
ap.add_argument("--mode", help="train/display/foto")
ap.add_argument("--path")
a = ap.parse_args()
mode = a.mode
path = a.path


def print_model_accuracy(model):
    """
    Plot Accuracy and Loss curves given the model_history
    """
    f, data = plt.subplots(1, 2, figsize=(15, 5))
    # summarize history for accuracy
    data[0].plot(range(1, len(model.history['acc']) + 1), model.history['acc'])
    data[0].plot(range(1, len(model.history['val_acc']) + 1), model.history['val_acc'])
    data[0].set_title('Model Accuracy')
    data[0].set_ylabel('Accuracy')
    data[0].set_xlabel('Epoch')
    data[0].set_xticks(np.arange(1, len(model.history['acc']) + 1), len(model.history['acc']) / 10)
    data[0].legend(['train', 'val'], loc='best')
    # summarize history for loss
    data[1].plot(range(1, len(model.history['loss']) + 1), model.history['loss'])
    data[1].plot(range(1, len(model.history['val_loss']) + 1), model.history['val_loss'])
    data[1].set_title('Model Loss')
    data[1].set_ylabel('Loss')
    data[1].set_xlabel('Epoch')
    data[1].set_xticks(np.arange(1, len(model.history['loss']) + 1), len(model.history['loss']) / 10)
    data[1].legend(['train', 'val'], loc='best')
    f.savefig('plot.png')
    plt.show()


# Define data generators
train_data_directory = 'data/train'
validation_data_directory = 'data/test'

num_train = 28709
num_val = 7178
batch_size = 64
num_epoch = 50

train_data_image_generator = ImageDataGenerator(rescale=1. / 255)
validation_data_image_generator = ImageDataGenerator(rescale=1. / 255)

train_generator = train_data_image_generator.flow_from_directory(
        train_data_directory,
        target_size=(48, 48),
        batch_size=batch_size,
        color_mode="grayscale",
        class_mode='categorical')

validation_generator = validation_data_image_generator.flow_from_directory(
        validation_data_directory,
        target_size=(48, 48),
        batch_size=batch_size,
        color_mode="grayscale",
        class_mode='categorical')

# Create the model
model = Sequential()

# 1st convolution layer
model.add(Conv2D(32, kernel_size=(3, 3), activation='relu', input_shape=(48, 48, 1)))
model.add(Conv2D(64, kernel_size=(3, 3), activation='relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))
model.add(Dropout(0.1))

model.add(Conv2D(128, kernel_size=(3, 3), activation='relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))
model.add(Conv2D(128, kernel_size=(3, 3), activation='relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))
model.add(Dropout(0.1))

model.add(Flatten())

# fully connected neural networks
model.add(Dense(512, activation='relu'))
model.add(Dropout(0.3))

model.add(Dense(7, activation='softmax'))

# If you want to train the same model or try other models, go for this
if mode == "train":
    model.compile(loss='categorical_crossentropy', optimizer=Adam(lr=0.0001, decay=1e-6), metrics=['accuracy'])

    model_info_history = model.fit_generator(
            train_generator,
            steps_per_epoch=num_train // batch_size,
            epochs=num_epoch,
            validation_data=validation_generator,
            validation_steps=num_val // batch_size)

    print_model_accuracy(model_info_history)
    model.save('model2.h5')

# emotions will be displayed on your face from the webcam feed
elif mode == "display":
    model = keras.models.load_model('model.h5')

    # prevents openCL usage and unnecessary logging messages
    cv2.ocl.setUseOpenCL(False)

    # dictionary which assigns each label an emotion (alphabetical order)
    emotions = {0: "Angry", 1: "Disgusted", 2: "Fearful", 3: "Happy", 4: "Neutral", 5: "Sad", 6: "Surprised"}

    # start the webcam feed
    cap = cv2.VideoCapture(0)
    while True:
        # Find haar cascade to draw bounding box around face
        ret, frame = cap.read()
        if not ret:
            break
        face_cascade = cv2.CascadeClassifier('haarcascade_frontalface_default.xml')
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        faces = face_cascade.detectMultiScale(gray, scaleFactor=1.3, minNeighbors=5)

        for (x, y, w, h) in faces:
            print(x, y, w, h)
            cv2.circle(frame, (x + int(w/2), y+70), int(w/2) + 30, (0, 255, 255), 5)
            # cv2.rectangle(frame, (x, y-50), (x+w, y+h+10), (0, 255, 255), 5)
            new_gray = gray[y:y + h, x:x + w]
            image = np.expand_dims(np.expand_dims(cv2.resize(new_gray, (48, 48)), -1), 0)
            prediction = model.predict(image)
            maxindex = int(np.argmax(prediction))
            cv2.putText(frame, emotions[maxindex], (x + 20, y - 60), cv2.FONT_HERSHEY_TRIPLEX, 1, (0, 0, 255), 2, cv2.LINE_AA)

        cv2.imshow('SI_Projekt', frame)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

    cap.release()
    cv2.destroyAllWindows()

elif mode == "foto":
    model = keras.models.load_model('model.h5')
    # prevents openCL usage and unnecessary logging messages
    cv2.ocl.setUseOpenCL(False)

    # dictionary which assigns each label an emotion (alphabetical order)
    emotions = {0: "Angry", 1: "Disgusted", 2: "Fearful", 3: "Happy", 4: "Neutral", 5: "Sad", 6: "Surprised"}

    # start the webcam feed
    cap = cv2.VideoCapture(0)
    while True:
        # Find haar cascade to draw bounding box around face
        ret, frame = cap.read()
        frame = cv2.imread(path)
        if not ret:
            break
        face_cascade = cv2.CascadeClassifier('haarcascade_frontalface_default.xml')
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        faces = face_cascade.detectMultiScale(gray, scaleFactor=1.3, minNeighbors=5)

        for (x, y, w, h) in faces:
            cv2.circle(frame, (x + int(w/2), y+100), int(w/2) + 50, (0, 255, 255), 5)
            # cv2.rectangle(frame, (x, y-50), (x+w, y+h+10), (0, 255, 255), 5)
            new_gray = gray[y:y + h, x:x + w]
            image = np.expand_dims(np.expand_dims(cv2.resize(new_gray, (48, 48)), -1), 0)
            prediction = model.predict(image)
            maxindex = int(np.argmax(prediction))
            cv2.putText(frame, emotions[maxindex], (x + 20, y + 30), cv2.FONT_HERSHEY_TRIPLEX, 1, (0,0,255), 2, cv2.LINE_AA)

        cv2.imshow('SI_Projekt', frame)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

    cap.release()
    cv2.destroyAllWindows()

elif mode == "summary":
    model = keras.models.load_model(path)
    model.compile(loss='categorical_crossentropy', optimizer=Adam(lr=0.0001, decay=1e-6), metrics=['accuracy'])

    Y_pred = model.predict_generator(validation_generator, num_val // batch_size + 1)
    y_pred = np.argmax(Y_pred, axis=1)
    conf_matrix = confusion_matrix(validation_generator.classes, y_pred)

    print(model.summary())
    print('Evaluation: ', model.evaluate_generator(validation_generator, steps=num_train // batch_size))
    print('Confusion Matrix')
    print(conf_matrix)
    sns.heatmap(conf_matrix)
    plt.show()
